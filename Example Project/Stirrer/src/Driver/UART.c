/*
 * UART.c
 *
 *  Created on: Nov 24, 2017
 *      Author: angusschmaloer
 *      This file is for the UART
 *      UART1:
 *      	Tx = PA9
 *      		 DMA CH4
 *      	Rx = PA10
 *      		 DMA CH5
// *    	UART3:
// *      	Tx = PB10
// *      		 DMA CH2
// *      	Rx = PB11
// *      		 DMA CH3
 *
 *	The UART driver consists of a initialization function and interrupt handlers.
 *	The UART drivers are static and not for variable use.
 *	UART 1 can be used for either modbus Slave or Master.
 */

#include "Driver/UART.h"
#include "User.h"

void HAL_UART_RxIdleCallback(UART_HandleTypeDef *huart);
void processMessage			(UART_HandleTypeDef *huart, uint16_t rxCount);

/*
 * The UART initialization function
 *	@pars:
 *		Serial_InitTypeDef: The Serial handle type
 *			it contains which UART
 *						the baudrate
 *						the stopbit
 *						the parity
 *						the wordlength
 *		UART_HandleTypeDef:	The UART handle type
 */
int UART_Init(Serial_InitTypeDef * Serial, UART_HandleTypeDef * UART_HandleStruct){
	GPIO_InitTypeDef   UART_GPIO_InitStruct;

	/* Check parameters*/
	if((Serial == NULL) || (UART_HandleStruct == NULL)){
		return 1;
	}

	/* Enable Clocks */
	__HAL_RCC_DMA1_CLK_ENABLE();
	__HAL_RCC_USART1_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();

	/* UART GPIO pin configuration  */
	UART_GPIO_InitStruct.Speed     	= GPIO_SPEED_FREQ_HIGH;
	UART_GPIO_InitStruct.Pull      	= GPIO_PULLUP;

	UART_GPIO_InitStruct.Mode      	= GPIO_MODE_AF_PP;
	UART_GPIO_InitStruct.Pin       	= GPIO_PIN_9; //tx
	HAL_GPIO_Init(GPIOA, &UART_GPIO_InitStruct);

	UART_GPIO_InitStruct.Mode      	= GPIO_MODE_INPUT;
	UART_GPIO_InitStruct.Pin       	= GPIO_PIN_10; //rx
	HAL_GPIO_Init(GPIOA, &UART_GPIO_InitStruct);

	/* UART init */
	UART_HandleStruct->Init.BaudRate 		= Serial->baudrate;
	UART_HandleStruct->Init.Mode			= UART_MODE_TX_RX;
	UART_HandleStruct->Init.WordLength 		= Serial->wordlength;
	UART_HandleStruct->Init.StopBits		= Serial->stopbit;
	//UART1_UART_HandleStruct.Init.Parity		= UART_PARITY_EVEN;			//!!! For the real
	UART_HandleStruct->Init.Parity			= Serial->parity;			//!!! For "Screen" only
	UART_HandleStruct->Init.HwFlowCtl		= UART_HWCONTROL_NONE;
	UART_HandleStruct->Init.OverSampling 	= UART_OVERSAMPLING_16;
	UART_HandleStruct->Instance				= Serial->UART;

	if(HAL_UART_Init(UART_HandleStruct) != HAL_OK){
		return 2;
	}

	/*
 	 * Added for the IDLE Flag
 	 */
	__HAL_UART_ENABLE_IT(UART_HandleStruct, UART_IT_IDLE);

	/* USART1 interrupt Enable */
	HAL_NVIC_SetPriority(USART1_IRQn, 3, 0);
	HAL_NVIC_EnableIRQ(USART1_IRQn);

	UART1_tx_DMA_HandleStruct.Instance 					= DMA1_Channel4;
	UART1_rx_DMA_HandleStruct.Instance 					= DMA1_Channel5;

	/* DMA init */
	UART1_tx_DMA_HandleStruct.Init.Direction 			= DMA_MEMORY_TO_PERIPH; //
	UART1_tx_DMA_HandleStruct.Init.PeriphInc 			= DMA_PINC_DISABLE;
	UART1_tx_DMA_HandleStruct.Init.MemInc 			    = DMA_MINC_ENABLE;
	UART1_tx_DMA_HandleStruct.Init.PeriphDataAlignment  = DMA_PDATAALIGN_BYTE;
	UART1_tx_DMA_HandleStruct.Init.MemDataAlignment 	= DMA_MDATAALIGN_BYTE;
	UART1_tx_DMA_HandleStruct.Init.Mode 				= DMA_NORMAL; //   DMA_NORMAL
	UART1_tx_DMA_HandleStruct.Init.Priority 			= DMA_PRIORITY_LOW;
	if(HAL_DMA_Init(&UART1_tx_DMA_HandleStruct) != HAL_OK){return 3;}

	UART1_rx_DMA_HandleStruct.Init.Direction 			= DMA_PERIPH_TO_MEMORY; //
	UART1_rx_DMA_HandleStruct.Init.PeriphInc 			= DMA_PINC_DISABLE;
	UART1_rx_DMA_HandleStruct.Init.MemInc 			    = DMA_MINC_ENABLE;
	UART1_rx_DMA_HandleStruct.Init.PeriphDataAlignment  = DMA_PDATAALIGN_BYTE;
	UART1_rx_DMA_HandleStruct.Init.MemDataAlignment 	= DMA_MDATAALIGN_BYTE;
	UART1_rx_DMA_HandleStruct.Init.Mode 				= DMA_CIRCULAR; //   DMA_NORMAL
	UART1_rx_DMA_HandleStruct.Init.Priority 			= DMA_PRIORITY_LOW;
	if(HAL_DMA_Init(&UART1_rx_DMA_HandleStruct) != HAL_OK){return 3;}

	/* DMA interrupt Enable */
	HAL_NVIC_SetPriority(DMA1_Channel4_IRQn, 6, 0);
	HAL_NVIC_EnableIRQ(DMA1_Channel4_IRQn);

	/* DMA interrupt Enable */
	HAL_NVIC_SetPriority(DMA1_Channel5_IRQn, 6, 0);
	HAL_NVIC_EnableIRQ(DMA1_Channel5_IRQn);

	__HAL_LINKDMA(UART_HandleStruct, hdmatx, UART1_tx_DMA_HandleStruct);
	__HAL_LINKDMA(UART_HandleStruct, hdmarx, UART1_rx_DMA_HandleStruct);

	__HAL_UART_FLUSH_DRREGISTER(UART_HandleStruct);


	return 0;
}

/* UART 1 IRQ handler */
void USART1_IRQHandler(void)
{
    HAL_NVIC_ClearPendingIRQ(USART1_IRQn);
    uint32_t tmp_flag = 0, tmp_it_source = 0;

#if UART1_MODBUS_MASTER

    /* call the interrupt handler */
    HAL_UART_IRQHandler(UART1_ModbusRTU_Master_Data_Struct->UART_Handle_Struct);

    /*
     * Added for IDLE Flag
     */
    tmp_flag = __HAL_UART_GET_FLAG(UART1_ModbusRTU_Master_Data_Struct->UART_Handle_Struct, UART_FLAG_IDLE);
    tmp_it_source = __HAL_UART_GET_IT_SOURCE(UART1_ModbusRTU_Master_Data_Struct->UART_Handle_Struct, UART_IT_IDLE);

    /* UART RX Idle interrupt --------------------------------------------*/
    if((tmp_flag != RESET) && (tmp_it_source != RESET))
    {
      __HAL_UART_CLEAR_IDLEFLAG(UART1_ModbusRTU_Master_Data_Struct->UART_Handle_Struct);
      HAL_UART_RxIdleCallback(UART1_ModbusRTU_Master_Data_Struct->UART_Handle_Struct);
    }

    /* Error handling */
    if(UART1_ModbusRTU_Master_Data_Struct->UART_Handle_Struct->ErrorCode != 0){
    	;;
    }
#endif
#if UART1_MODBUS_SLAVE

    /* call the interrupt handler */
    HAL_UART_IRQHandler(UART1_ModbusRTU_Slave_Data_Struct->UART_Handle_Struct);

    /*
     * Added for IDLE Flag
     */
    tmp_flag = __HAL_UART_GET_FLAG(UART1_ModbusRTU_Slave_Data_Struct->UART_Handle_Struct, UART_FLAG_IDLE);
    tmp_it_source = __HAL_UART_GET_IT_SOURCE(UART1_ModbusRTU_Slave_Data_Struct->UART_Handle_Struct, UART_IT_IDLE);

    /* UART RX Idle interrupt --------------------------------------------*/
    if((tmp_flag != RESET) && (tmp_it_source != RESET))
    {
      HAL_UART_RxIdleCallback(UART1_ModbusRTU_Slave_Data_Struct->UART_Handle_Struct);
      __HAL_UART_CLEAR_IDLEFLAG(UART1_ModbusRTU_Slave_Data_Struct->UART_Handle_Struct);

    }

    /* Error handling */
    if(UART1_ModbusRTU_Slave_Data_Struct->UART_Handle_Struct->ErrorCode != 0){
    	;;
    }
#endif
}

/**
  * @brief  UART receive process idle callback for short RX DMA transfers.
  * @param  huart: Pointer to a UART_HandleTypeDef structure that contains
  *                the configuration information for the specified UART module.
  * @retval None
  */
void HAL_UART_RxIdleCallback(UART_HandleTypeDef* huart)
{
  uint16_t rxXferCount = 0;
  if(huart->hdmarx != NULL)
  {
    DMA_HandleTypeDef *hdma = huart->hdmarx;

    /* Determine how many items of data have been received */
    rxXferCount = huart->RxXferSize - __HAL_DMA_GET_COUNTER(hdma);

    HAL_DMA_Abort(huart->hdmarx);

    huart->RxXferCount = 0;
    /* Check if a transmit process is ongoing or not */
    if(huart->gState == HAL_UART_STATE_BUSY_TX_RX)
    {
      huart->gState = HAL_UART_STATE_BUSY_TX;
    }
    else
    {
      huart->gState = HAL_UART_STATE_READY;
    }
  }
  processMessage(huart, rxXferCount);
}

/**
  * @brief  UART processMessage, called when idle interrupt is triggers.
  * @param  huart: Pointer to a UART_HandleTypeDef structure that contains
  *                the configuration information for the specified UART module.
  *         rxCount: the number of chars received
  * @retval None
  */
void processMessage(UART_HandleTypeDef *huart, uint16_t rxCount){
	if(huart->Instance == USART1){
	#if UART1_MODBUS_MASTER
		/* stop the DMA */
		HAL_UART_DMAStop(UART1_ModbusRTU_Master_Data_Struct->UART_Handle_Struct);
		__HAL_UART_FLUSH_DRREGISTER(UART1_ModbusRTU_Master_Data_Struct->UART_Handle_Struct); 					    // Clear the buffer to prevent overrun
		/* Call the modbus Master interrupt handler */
		ModbusRTU_Master_Handler(UART1_ModbusRTU_Master_Data_Struct, rxCount);
	#endif
	#if UART1_MODBUS_SLAVE
		//HAL_UART_DMAStop(UART1_ModbusRTU_Master_Data_Struct->UART_Handle_Struct);
		__HAL_UART_FLUSH_DRREGISTER(UART1_ModbusRTU_Slave_Data_Struct->UART_Handle_Struct); 					    // Clear the buffer to prevent overrun
		/* Call the modbus Slave interrupt handler */
		if(ModbusRTU_Slave_Handler(UART1_ModbusRTU_Slave_Data_Struct, rxCount) != HAL_OK){
			/* if not OK, then stop and restart */
			HAL_UART_DMAStop(UART1_ModbusRTU_Slave_Data_Struct->UART_Handle_Struct);
			if(HAL_UART_Receive_DMA(UART1_ModbusRTU_Slave_Data_Struct->UART_Handle_Struct, UART1_ModbusRTU_Slave_Data_Struct->Data_String, 256) != HAL_OK){
				;;
			}
		}
	#endif
	}
}

/**
  * @brief  UART Transmit callback function.
  * @param  huart: Pointer to a UART_HandleTypeDef structure that contains
  *                the configuration information for the specified UART module.
  * @retval None
  */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart->Instance == USART1){
#if UART1_MODBUS_MASTER
		/* Stop the DMA */
		HAL_UART_DMAStop(UART1_ModbusRTU_Master_Data_Struct->UART_Handle_Struct);
		/* Get the current systick */
		UART1_ModbusRTU_Master_Data_Struct->TimeoutCount = HAL_GetTick();
		/* Set the flag to transmitted */
		(UART1_ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct + UART1_ModbusRTU_Master_Data_Struct->QueueCounter)->status = transmitted;
		/* Set the UART to receive data via the DMA */
		if(HAL_UART_Receive_DMA(UART1_ModbusRTU_Master_Data_Struct->UART_Handle_Struct, UART1_ModbusRTU_Master_Data_Struct->Data_String, 256) != HAL_OK){
			;;
		}
#endif
#if UART1_MODBUS_SLAVE
		/* Stop the DMA */
		HAL_UART_DMAStop(UART1_ModbusRTU_Slave_Data_Struct->UART_Handle_Struct);
		/* Set the UART to receive data via the DMA */
		if(HAL_UART_Receive_DMA(UART1_ModbusRTU_Slave_Data_Struct->UART_Handle_Struct, UART1_ModbusRTU_Slave_Data_Struct->Data_String, 256) != HAL_OK){
			;;
		}
#endif
	}
}

/**
  * @brief  UART Error callback function.
  * @param  huart: Pointer to a UART_HandleTypeDef structure that contains
  *                the configuration information for the specified UART module.
  * @retval None
  */
void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart){
#if UART1_MODBUS_MASTER
	if((huart->ErrorCode & UART_FLAG_ORE) != RESET){ //Overrun error
		UART1_ModbusRTU_Master_Data_Struct->Error_Counters.CPT8++;
		huart->ErrorCode &= !UART_FLAG_ORE;
	}
	if((huart->ErrorCode & UART_FLAG_NE) != RESET){ //Noise Error
		huart->ErrorCode &= !UART_FLAG_NE;
	}
	if((huart->ErrorCode & UART_FLAG_FE) != RESET){ //Frame Error
		UART1_ModbusRTU_Master_Data_Struct->Error_Counters.CPT2++;
		huart->ErrorCode &= !UART_FLAG_FE;
	}
	if((huart->ErrorCode & UART_FLAG_PE) != RESET){ //Parity error
		huart->ErrorCode &= !UART_FLAG_PE;
	}
#endif
#if UART1_MODBUS_SLAVE
	;;
#endif
}





