/*
 * I2C.c
 *
 *  Created on: Dec 8, 2017
 *      Author: angusschmaloer
 *
 *	This is the I2C driver.
 *	It has the I2C Init and the Queue implementation
 */

#include "Driver/I2C.h"

#define I2C1_CLK_ENABLE() 		__HAL_RCC_I2C1_CLK_ENABLE()
#define I2C1_GPIO_CLK_ENABLE()	__HAL_RCC_GPIOB_CLK_ENABLE()
#define I2C1_FORCE_RESET() 		__HAL_RCC_I2C1_FORCE_RESET()
#define I2C1_RELEASE_RESET() 	__HAL_RCC_I2C1_RELEASE_RESET()
#define DMA1_CLK_ENABLE()		__HAL_RCC_DMA1_CLK_ENABLE()

DMA_HandleTypeDef hdma_tx;
DMA_HandleTypeDef hdma_rx;

int Errors[8];

I2C_Queue_struct *  FirstAction 		= NULL;
I2C_Queue_struct *  LastAction			= NULL;
volatile int Queue_Size 				= 0;
int TimeoutTimerCount 					= 0;
/* Contains the total size of the allocated data */
int totsize								= 0;

int  I2C_Handle_Queue(void);

/* This counter contains the value of the total I2C commands ever added to the queue */
int I2C_cnt = 0;


/*
 *	Checks The timeout Time.
 *	This function is called every 1 Ms
 */

void I2C_Master_Timeout_Handler(void){
	I2C_Queue_struct *temp_I2C_Queue_struct;
	/* Check if the timeout timer is being enabled */
	if(FirstAction->TimeoutTimer == true){
		TimeoutTimerCount++;
		if(TimeoutTimerCount == 1000){

			/* Disable interrupts, so that the process wont be interrupted */
			HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
			HAL_NVIC_DisableIRQ(TIM4_IRQn);

			/* The timeout counter is done counting now and can be disabled */
			FirstAction->TimeoutTimer = false;

			/* If the message is a write and isn't NULL, then the data can be freed */
			if(FirstAction->Direction == transmit){
				if(FirstAction->pData == 0x00){
					Errors[0]++;
				}else{
					/* free data and remove the size from totsize */
					totsize -= (sizeof(uint8_t)* FirstAction->Size);
					free(FirstAction->pData);
				}
			}


			/* The Next_I2C_Queue_struct should be the new first and the old one can be freed */
			temp_I2C_Queue_struct = FirstAction;
			FirstAction = temp_I2C_Queue_struct->Next_I2C_Queue_struct;
			if(temp_I2C_Queue_struct == 0x00){
				Errors[1]++;
			}else{
				/* free the data structure and remove the size from totsize */
				totsize -= sizeof(temp_I2C_Queue_struct);
				free(temp_I2C_Queue_struct);
			}
			Queue_Size--;

			/* Enable the interrupts again */
		    HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
			HAL_NVIC_EnableIRQ(TIM4_IRQn);

			/* Call the Handle function so the next command can be send */
			I2C_Handle_Queue();
		}
	}
}

/*
 * This is the function where new I2C commands should be added to the queue.
 * It allocates memory for the new I2C data structure and for the data, because of the life span of data in send functions.
 * Only allocated memory for data when it is not used for receiving data.
 * When receiving, the user should give up its own pointer where the data should be put.
 *
 *	@pars:
 *		I2C_HandleTypeDef: 	The I2c handle type
 *		DevAddress:			The address of the slave
 *		pData:				The pointer to the data that should be send or the data where the received data should be put.
 *		Size:				The size of the data.
 *		Direction:			The data communication direction, can be transmit or receive.
 *		Flag:				The I2C flag, it can be NULL is you don't want the use a flag.
 */
int I2C_Master_Communication(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData, uint16_t Size, enum Direction Direction, enum I2C_Flag *Flag){
	I2C_Queue_struct * new_I2C_Queue_struct;
	uint8_t 		 * newData;
	int returnvalue = 0;

	/*  */
	I2C_cnt++;

	if(Queue_Size == MAXQUEUESIZE){
		return 1;
	}

	/* Disable the interrupts so that the process will not be interrupted */
	HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
	HAL_NVIC_DisableIRQ(TIM4_IRQn);
	HAL_NVIC_DisableIRQ(I2C1_EV_IRQn);

	/* Allocate new data for the queue */
	if ((new_I2C_Queue_struct = (I2C_Queue_struct *)calloc(1, sizeof(I2C_Queue_struct))) == NULL){
		HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
		HAL_NVIC_EnableIRQ(TIM4_IRQn);
		HAL_NVIC_EnableIRQ(I2C1_EV_IRQn);

		Errors[2]++;
		return 2;
	}
	totsize += sizeof(I2C_Queue_struct);
	/* If a message should be send, then allocate new memory and copy the data into the memory. */
	if(Direction == transmit){
		if ((newData = (uint8_t *)calloc(Size, sizeof(uint8_t))) == NULL){
			HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
			HAL_NVIC_EnableIRQ(TIM4_IRQn);
			HAL_NVIC_EnableIRQ(I2C1_EV_IRQn);

			Errors[3]++;
			return 3;
		}
		/* add the size of the new allocated data to the totsize int */

		totsize += (sizeof(uint8_t)*Size);

		/* copy the data */
		for(int x = 0; x < Size; x++){
			*(newData + x) = *(pData+ x);
		}
	}else{
		if (pData == NULL){
			HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
			HAL_NVIC_EnableIRQ(TIM4_IRQn);
			HAL_NVIC_EnableIRQ(I2C1_EV_IRQn);

			Errors[3]++;
			return 3;
		}
	}

	/* set the data int the new data structure */
	new_I2C_Queue_struct->DevAddress		= DevAddress;
    new_I2C_Queue_struct->Size				= Size;
	new_I2C_Queue_struct->hi2c				= hi2c;
	new_I2C_Queue_struct->pData				= newData;
	new_I2C_Queue_struct->Direction			= Direction;
	new_I2C_Queue_struct->Flag				= Flag;
	new_I2C_Queue_struct->TimeoutTimer		= false;
	new_I2C_Queue_struct->Next_I2C_Queue_struct = NULL;

	/* set flag */
	if(new_I2C_Queue_struct->Flag != NULL){
		*FirstAction->Flag = Waiting;
	}

	/* Add the new structure into the queue */
	if(Queue_Size == 0){
		FirstAction = new_I2C_Queue_struct;
		LastAction  = new_I2C_Queue_struct;
	}else{
	    LastAction->Next_I2C_Queue_struct = new_I2C_Queue_struct;
	    // Designate the new Action as the new lastAction:
	    LastAction = new_I2C_Queue_struct;
	}

    Queue_Size++;

	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
	HAL_NVIC_EnableIRQ(TIM4_IRQn);
	HAL_NVIC_EnableIRQ(I2C1_EV_IRQn);

	/* If its the first command, then start the handling of the queue */
    if(Queue_Size == 1){
    	returnvalue = I2C_Handle_Queue();
    }

    return returnvalue;
}

/*
 * Handles the queue, sends the next command.
 *
 */
int I2C_Handle_Queue(void){
	if(Queue_Size == 0){
		return 1;
	}

	if(FirstAction == NULL){
		return 2;
	}

	/* set the timeout timer and send */
	if(FirstAction->Direction == transmit){
		FirstAction->TimeoutTimer = true;
		TimeoutTimerCount = 0;
		if(HAL_I2C_Master_Transmit_DMA(FirstAction->hi2c, FirstAction->DevAddress, FirstAction->pData, FirstAction->Size) != HAL_OK){
			Errors[4]++;
		}
	}else if(FirstAction->Direction == receive){
		FirstAction->TimeoutTimer = true;
		TimeoutTimerCount = 0;
		HAL_I2C_Master_Receive_DMA(FirstAction->hi2c, FirstAction->DevAddress, FirstAction->pData, FirstAction->Size);
	}
	return 0;
}

/*
 * The transfer complete callback function
 */
void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *I2cHandle)
{
	I2C_Queue_struct *temp_I2C_Queue_struct;

	if(I2cHandle->Instance == I2C1){
		/*
		 * if(FirstAction->Direction == transmit),
		 * then the command is done and can be removed, and the next can be send
		 * */
		if(FirstAction->Direction == transmit){

			HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
			HAL_NVIC_DisableIRQ(TIM4_IRQn);
			HAL_NVIC_DisableIRQ(I2C1_EV_IRQn);

			/* stop the timeout counter */
			FirstAction->TimeoutTimer = false;

			/* set flag */
			if(FirstAction->Flag != NULL){
				*FirstAction->Flag = Transmitted;
			}

			/* free data */
			if(FirstAction->pData == 0x00){
				Errors[5]++;
			}else{
				/* remove the size of the handled data in the totsize int */
				totsize -= (sizeof(uint8_t)* FirstAction->Size);
				free(FirstAction->pData);
			}

			/* Remove the first structure form the queue */
			temp_I2C_Queue_struct = FirstAction;
			FirstAction = temp_I2C_Queue_struct->Next_I2C_Queue_struct;

			/* free the removed data*/
			if(temp_I2C_Queue_struct == 0x00){
				Errors[6]++;
			}else{
				/* remove the size of the handled data in the totsize int */
				totsize -= sizeof(*FirstAction);
				free(temp_I2C_Queue_struct);
			}

			Queue_Size--;

		    HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
			HAL_NVIC_EnableIRQ(TIM4_IRQn);
			HAL_NVIC_EnableIRQ(I2C1_EV_IRQn);

			/* were done and can handle the next I2C structure */
			I2C_Handle_Queue();
		}
		/*
		 * if(FirstAction->Direction == receive)
		 * Then the request is send and we wait till an answer.
		 * */
		else if(FirstAction->Direction == receive){
			TimeoutTimerCount = 0;
			if(FirstAction->Flag != NULL){
				*FirstAction->Flag = Transmitted;
			}
		}
	}
}

/*
 * The receive complete callback function
 */
void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *I2cHandle)
{
	I2C_Queue_struct *temp_I2C_Queue_struct;
	if(I2cHandle->Instance == I2C1){
		/*
		 * We don't expect received data and do notting.
		 * An error handler could be added.
		 * */
		if(FirstAction->Direction == transmit){

			//Error

		}
		/*
		 * if(FirstAction->Direction == receive)
		 * We received the data.
		 *
		 * */
		else if(FirstAction->Direction == receive){

			HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
			HAL_NVIC_DisableIRQ(TIM4_IRQn);
			HAL_NVIC_DisableIRQ(I2C1_EV_IRQn);

			FirstAction->TimeoutTimer = false;

			if(FirstAction->Flag != NULL){
				*FirstAction->Flag = Received;
			}

			/* Remove the first structure form the queue */
			temp_I2C_Queue_struct = FirstAction;
			FirstAction = temp_I2C_Queue_struct->Next_I2C_Queue_struct;

			if(temp_I2C_Queue_struct == 0x00){
				Errors[7]++;
			}else{
				/* remove the size of the handled data in the totsize int */
				totsize -= sizeof(*FirstAction);
				free(temp_I2C_Queue_struct);
			}

			Queue_Size--;

		    HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
			HAL_NVIC_EnableIRQ(TIM4_IRQn);
			HAL_NVIC_EnableIRQ(I2C1_EV_IRQn);

			I2C_Handle_Queue();
		}
	}
}


void I2C_Init(I2C_HandleTypeDef *I2C_HandleStruct){
	GPIO_InitTypeDef  I2C1_GPIO_InitStruct;

	I2C1_I2C_HandleStruct = I2C_HandleStruct;

	I2C1_CLK_ENABLE();
	I2C1_GPIO_CLK_ENABLE();
	DMA1_CLK_ENABLE();

	I2C1_FORCE_RESET();
	I2C1_RELEASE_RESET();

	I2C1_GPIO_InitStruct.Mode      	= GPIO_MODE_AF_PP;
	I2C1_GPIO_InitStruct.Pull      	= GPIO_PULLDOWN;
	I2C1_GPIO_InitStruct.Speed     	= GPIO_SPEED_FREQ_HIGH;
	I2C1_GPIO_InitStruct.Pin       	= GPIO_PIN_6 | GPIO_PIN_7;
	HAL_GPIO_Init(GPIOB, &I2C1_GPIO_InitStruct);

	/*##-3- Configure the DMA Channels #########################################*/
	/* Configure the DMA handler for Transmission process */
	hdma_tx.Instance                 = DMA1_Channel6;
	hdma_tx.Init.Direction           = DMA_MEMORY_TO_PERIPH;
	hdma_tx.Init.PeriphInc           = DMA_PINC_DISABLE;
	hdma_tx.Init.MemInc              = DMA_MINC_ENABLE;
	hdma_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	hdma_tx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
	hdma_tx.Init.Mode                = DMA_NORMAL;
	hdma_tx.Init.Priority            = DMA_PRIORITY_LOW;

	HAL_DMA_Init(&hdma_tx);

	/* Associate the initialized DMA handle to the the I2C handle */
	__HAL_LINKDMA(I2C1_I2C_HandleStruct, hdmatx, hdma_tx);

	/* Configure the DMA handler for Transmission process */
	hdma_rx.Instance                 = DMA1_Channel7;
	hdma_rx.Init.Direction           = DMA_PERIPH_TO_MEMORY;
	hdma_rx.Init.PeriphInc           = DMA_PINC_DISABLE;
	hdma_rx.Init.MemInc              = DMA_MINC_ENABLE;
	hdma_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	hdma_rx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
	hdma_rx.Init.Mode                = DMA_NORMAL;
	hdma_rx.Init.Priority            = DMA_PRIORITY_HIGH;

	HAL_DMA_Init(&hdma_rx);

	/* Associate the initialized DMA handle to the the I2C handle */
	__HAL_LINKDMA(I2C1_I2C_HandleStruct, hdmarx, hdma_rx);

	/*##-4- Configure the NVIC for DMA #########################################*/
	/* NVIC configuration for DMA transfer complete interrupt (I2Cx_TX) */
	HAL_NVIC_SetPriority(DMA1_Channel6_IRQn, 2, 1);
	HAL_NVIC_EnableIRQ(DMA1_Channel6_IRQn);

	/* NVIC configuration for DMA transfer complete interrupt (I2Cx_RX) */
	HAL_NVIC_SetPriority(DMA1_Channel7_IRQn, 2, 0);
	HAL_NVIC_EnableIRQ(DMA1_Channel7_IRQn);

	/*##-5- Configure the NVIC for I2C #########################################*/
	/* NVIC for I2C1 */
	HAL_NVIC_SetPriority(I2C1_ER_IRQn, 2, 1);
	HAL_NVIC_EnableIRQ(I2C1_ER_IRQn);

	HAL_NVIC_SetPriority(I2C1_EV_IRQn, 2, 2);
	HAL_NVIC_EnableIRQ(I2C1_EV_IRQn);

	I2C1_I2C_HandleStruct->Instance              = I2C1;
	I2C1_I2C_HandleStruct->Init.ClockSpeed		 = 100000;
	I2C1_I2C_HandleStruct->Init.OwnAddress1      = 0;
	I2C1_I2C_HandleStruct->Init.AddressingMode   = I2C_ADDRESSINGMODE_7BIT;
	I2C1_I2C_HandleStruct->Init.DualAddressMode  = I2C_DUALADDRESS_DISABLE;
	I2C1_I2C_HandleStruct->Init.OwnAddress2      = 0;
	I2C1_I2C_HandleStruct->Init.GeneralCallMode  = I2C_GENERALCALL_DISABLE;
	I2C1_I2C_HandleStruct->Init.NoStretchMode    = I2C_NOSTRETCH_DISABLE;

	HAL_I2C_Init(I2C1_I2C_HandleStruct);
}
