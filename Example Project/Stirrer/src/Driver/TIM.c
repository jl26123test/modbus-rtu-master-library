/*
 * TIM.c
 *
 *  Created on: Dec 14, 2017
 *      Author: angusschmaloer
 */

#include "Driver/TIM.h"
#include "stm32f1xx_hal_rcc.h"
#include "stm32f1xx_hal.h"

TIM_OC_InitTypeDef	TIM2_Channel_1_OCStruct;

/*
 * Initialize timer 2, used for the PWM
 * */
void TIM2_Init(TIM_HandleTypeDef *htim){
	GPIO_InitTypeDef 	GPIO_InitStruct;
		__HAL_RCC_GPIOA_CLK_ENABLE();
		__HAL_RCC_GPIOB_CLK_ENABLE();

		/* Polarity pin */
		GPIO_InitStruct.Pin 		= PolarityPin;
		GPIO_InitStruct.Mode 		= GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull 		= GPIO_PULLDOWN;
		GPIO_InitStruct.Speed		= GPIO_SPEED_FREQ_HIGH;
		HAL_GPIO_Init(PolarityPORT, &GPIO_InitStruct);
		HAL_GPIO_WritePin(PolarityPORT, PolarityPin, 1);

		/* Polarity pin */
		GPIO_InitStruct.Pin 		= EnablePin;
		GPIO_InitStruct.Mode 		= GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull 		= GPIO_PULLDOWN;
		GPIO_InitStruct.Speed		= GPIO_SPEED_FREQ_HIGH;
		HAL_GPIO_Init(EnablePORT, &GPIO_InitStruct);
		HAL_GPIO_WritePin(EnablePORT, EnablePin, 1);

		/* PWN Signal */
		GPIO_InitStruct.Pin 		= PWMPin;
		GPIO_InitStruct.Mode 		= GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull 		= GPIO_PULLUP;
		GPIO_InitStruct.Speed		= GPIO_SPEED_FREQ_HIGH;
		HAL_GPIO_Init(PWMPORT, &GPIO_InitStruct);


		/* TIM2_CH1 */
		__HAL_RCC_TIM2_CLK_ENABLE();
		htim->Instance 					= TIM2;
		htim->Init.Prescaler 			= (2) - 1; 					//8 000 000 / 25 = 320 000 Hz
		htim->Init.CounterMode 			= TIM_COUNTERMODE_UP;
		htim->Init.Period 				= (10000*4) - 1; 					//Period in us
		htim->Init.ClockDivision 		= TIM_CLOCKDIVISION_DIV1;
		htim->Init.RepetitionCounter 	= 0;

		HAL_TIM_PWM_Init(htim);

		TIM2_Channel_1_OCStruct.OCMode					= TIM_OCMODE_PWM1;
		TIM2_Channel_1_OCStruct.Pulse					= (0);

		HAL_TIM_PWM_ConfigChannel(htim, &TIM2_Channel_1_OCStruct, TIM_CHANNEL_1);	//us
}

/*
 * Initialize timer 4, used for the stirrer timer
 * */
void TIM4_Init(TIM_HandleTypeDef *htim){
	/* TIM4_CH1 */
	__HAL_RCC_TIM4_CLK_ENABLE();
	htim->Instance 					= TIM4;
	htim->Init.Prescaler 			= (8000000/100000) - 1; 					//10KHz
	htim->Init.CounterMode 			= TIM_COUNTERMODE_UP;
	htim->Init.Period 				= (100000/10000) - 1;
	htim->Init.ClockDivision 		= TIM_CLOCKDIVISION_DIV1;
	htim->Init.RepetitionCounter	= 0;

	HAL_TIM_Base_Init(htim);

	HAL_NVIC_SetPriority(TIM4_IRQn, 15, 0);
	HAL_NVIC_EnableIRQ(TIM4_IRQn);
}



