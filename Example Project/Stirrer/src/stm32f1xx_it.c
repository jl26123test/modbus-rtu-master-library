/**
  ******************************************************************************
  * @file    stm32f1xx_it.c
  * @author  Ac6
  * @version V1.0
  * @date    02-Feb-2015
  * @brief   Default Interrupt Service Routines.
  ******************************************************************************
*/

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"
#include "stm32f1xx.h"
#ifdef USE_RTOS_SYSTICK
#include <cmsis_os.h>
#endif
#include "stm32f1xx_it.h"

#include "Driver/I2C.h"
#include "User/Timer.h"
#include "Driver/UART.h"
#include "User.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            	  	    Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler, but only if no RTOS defines it.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
	HAL_IncTick();
	HAL_SYSTICK_IRQHandler();
#ifdef USE_RTOS_SYSTICK
	osSystickHandler();
#endif
}

void EXTI9_5_IRQHandler(void){
  /* EXTI line interrupt detected */
  if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_5) != RESET)
  {
	HAL_GPIO_EXTI_Callback(GPIO_PIN_5);
	__HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_5);
  }else if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_6) != RESET)
  {
    //HAL_GPIO_EXTI_Callback(GPIO_PIN_6);
	__HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_6);
  }else if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_7) != RESET)
  {
    HAL_GPIO_EXTI_Callback(GPIO_PIN_7);
    __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_7);
  }else if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_8) != RESET)
  {
	HAL_GPIO_EXTI_Callback(GPIO_PIN_8);
	__HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_8);
  }
}

void I2C1_EV_IRQHandler(void)
{
  HAL_I2C_EV_IRQHandler(I2C1_I2C_HandleStruct);
  HAL_NVIC_ClearPendingIRQ(I2C1_EV_IRQn);
}

void I2C1_ER_IRQHandler(void)
{
  HAL_I2C_ER_IRQHandler(I2C1_I2C_HandleStruct);
  HAL_NVIC_ClearPendingIRQ(I2C1_ER_IRQn);
}

/* I2C Rx */
void DMA1_Channel7_IRQHandler(void)
{
  HAL_DMA_IRQHandler(I2C1_I2C_HandleStruct->hdmarx);
  HAL_NVIC_ClearPendingIRQ(DMA1_Channel7_IRQn);
}
/* I2C Tx */
void DMA1_Channel6_IRQHandler(void)
{
  HAL_DMA_IRQHandler(I2C1_I2C_HandleStruct->hdmatx);
  HAL_NVIC_ClearPendingIRQ(DMA1_Channel6_IRQn);
}

/* UART1 Tx */
void DMA1_Channel4_IRQHandler(void)
{
  HAL_DMA_IRQHandler(&UART1_tx_DMA_HandleStruct);
  HAL_NVIC_ClearPendingIRQ(DMA1_Channel4_IRQn);
}

/* UART1 Rx */
void DMA1_Channel5_IRQHandler(void)
{
  HAL_DMA_IRQHandler(&UART1_rx_DMA_HandleStruct);
  HAL_NVIC_ClearPendingIRQ(DMA1_Channel5_IRQn);
}

void TIM4_IRQHandler()
{
	HAL_TIM_IRQHandler(&TIM4_Handle);
}


