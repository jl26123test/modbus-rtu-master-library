/*
 * User.c
 *
 *  Created on: Dec 8, 2017
 *      Author: angusschmaloer
 *
 *	This is the User file where the basic Init functions are called.
 */

#include "User.h"

void MILI_SECONDS_SYSTICK_Callback();
void TEN_MILI_SECONDS_SYSTICK_Callback();
void HUNDRED_MILI_SECONDS_SYSTICK_Callback();
void SECONDS_SYSTICK_Callback();

#define __HoldingRegisterssize	100
#define __CoilsSize				100
#define __InputSize				100
#define __DiscreteInputSize		100

ModbusRTU_Queue_Typedef		ModbusRTU_Queue_Struct[12];
uint16_t data = 5555;

Modbus_Slave_Data_Typedef	Modbus_Slave_Data_Struct;
uint16_t	HoldingRegesters	[__HoldingRegisterssize];
uint16_t	Inputs				[__CoilsSize];
uint8_t		Discrete_Inputs		[__InputSize];
bool 		Coils				[__DiscreteInputSize];

void User_UART_Init(UART_HandleTypeDef * UART_HandleStruct){
	Serial_InitTypeDef  Serial;
	Serial.UART 		= USART1;
	Serial.baudrate 	= 9600;
	Serial.stopbit 		= UART_STOPBITS_1;
	Serial.parity		= UART_PARITY_NONE;
	Serial.wordlength	= UART_WORDLENGTH_8B;
	if(UART_Init(&Serial, UART_HandleStruct) != 0){
		//error;
	}
}

void User_ModbusRTU_Master_Init(ModbusRTU_Master_Data_Typedef *ModbusRTU_Master_Data_Struct, UART_HandleTypeDef *UART_HandleStruct){
	for(int x = 0; x < 12; x++){
		ModbusRTU_Queue_Struct[x].active 		 = true;
		ModbusRTU_Queue_Struct[x].continuous 	 = true;
		ModbusRTU_Queue_Struct[x].retries		 = 1;
		ModbusRTU_Queue_Struct[x].timeout		 = 10;
		ModbusRTU_Queue_Struct[x].times_failed	 = 0;	//Set to 0
		ModbusRTU_Queue_Struct[x].status		 = 0;	//Set to 0

		ModbusRTU_Queue_Struct[x].Slave_Address  = 1;
		ModbusRTU_Queue_Struct[x].Function_Codes = Write_Single_Register;
		ModbusRTU_Queue_Struct[x].Address		 = x;
		ModbusRTU_Queue_Struct[x].Quantity		 = 1;
		ModbusRTU_Queue_Struct[x].data16		 = &data;
	}

	ModbusRTU_Master_Data_Struct->UART_Handle_Struct 	 = UART_HandleStruct;
	ModbusRTU_Master_Data_Struct->ModbusRTU_Queue_Struct = ModbusRTU_Queue_Struct;
	ModbusRTU_Master_Data_Struct->queueSize				 = 12;
	ModbusRTU_Master_Init(ModbusRTU_Master_Data_Struct);
}

void User_ModbusRTU_Slave_Init(ModbusRTU_Slave_Data_Typedef *ModbusRTU_Slave_Data_Struct, UART_HandleTypeDef *UART_HandleStruct){
	Modbus_Slave_Data_Struct.Modbus_Slave_Holding_Registers = HoldingRegesters;
	Modbus_Slave_Data_Struct.Holding_Registers_size 		= __HoldingRegisterssize;
	Modbus_Slave_Data_Struct.Modbus_Slave_Coils 			= Coils;
	Modbus_Slave_Data_Struct.Coils_Size 					= __CoilsSize;
	Modbus_Slave_Data_Struct.Modbus_Slave_Inputs 			= Inputs;
	Modbus_Slave_Data_Struct.Input_Size 					= __InputSize;
	Modbus_Slave_Data_Struct.Modbus_Slave_Discrete_Inputs 	= Discrete_Inputs;
	Modbus_Slave_Data_Struct.Discrete_Input_Size 			= __DiscreteInputSize;

	ModbusRTU_Slave_Data_Struct->Modbus_Slave_Data 	= &Modbus_Slave_Data_Struct;
	ModbusRTU_Slave_Data_Struct->Slave_Address 		= 1;
	ModbusRTU_Slave_Data_Struct->UART_Handle_Struct = UART_HandleStruct;

	ModbusRTU_Slave_Init(ModbusRTU_Slave_Data_Struct);
	ModbusRTU_Slave_Start(ModbusRTU_Slave_Data_Struct);
}

void Display_Init(I2C_HandleTypeDef *I2C_HandleStruct){
	  SSD1306_Init(I2C_HandleStruct);
	  SSD1306DisplaySetupState();
	  SSD1306DisplaySetupRPM();
	  SSD1306DisplaySetupTime();
	  SSD1306DisplaySetupStart();
	  SSD1306DisplayMenuPointer(0,1);
}

void Timer_Init(void){
	TIM2_Initialization();
	TIM4_Initialization();
}

void RotaryEncoder_Init(void)
{
	RotaryEncoderInit();
}

/*
 * TIM Period Elapsed Callback function
 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim->Instance == TIM2){
		;
	}
	else if(htim->Instance == TIM4){
		Timer4_Callback();
	}
}



/*
 * This Interrupt will occur every Mili Second
 * The user can use this as its interrupt function to call other interrupts
 */
void MILI_SECONDS_SYSTICK_Callback(){
#if UART1_MODBUS_MASTER
	ModbusRTU_Master_Timeout_Handler();
#endif
#if I2C1_Master
	I2C_Master_Timeout_Handler();			//Checks the I2C Timeout time
#endif

}

/*
 * This Interrupt will occur every Ten Mili Second
 * The user can use this as its interrupt function to call other interrupts
 */
void TEN_MILI_SECONDS_SYSTICK_Callback()
{

}

/*
 * This Interrupt will occur every Hundred Mili Second
 * The user can use this as its interrupt function to call other interrupts
 */
void HUNDRED_MILI_SECONDS_SYSTICK_Callback()
{

}

/*
 * This Interrupt will occur every Second
 * The user can use this as its interrupt function to call other interrupts
 */
void SECONDS_SYSTICK_Callback(){
#if I2C1_Master
	ModbusRTU_Master_Start_Queue(ModbusRTU_Master_Data_Struct); //Start the Modbus MAster communication every second, for updating
#endif
}



/*
 * Function that occurs every 1ms
 * The user can use this as its interrupt function to call other interrupts
 */
void HAL_SYSTICK_Callback(void)
{
	static uint8_t  Mili_Second_Count;
	static uint8_t  Ten_Mili_Second_Count;
	static uint8_t  Hundred_Mili_Second_Count;
	static uint8_t  Second_Count;

	Mili_Second_Count++;
	if(Mili_Second_Count >=10){
		Mili_Second_Count = 0;
		MILI_SECONDS_SYSTICK_Callback();
		Ten_Mili_Second_Count++;

		if(Ten_Mili_Second_Count >=10){
			Ten_Mili_Second_Count = 0;

			TEN_MILI_SECONDS_SYSTICK_Callback();
			Hundred_Mili_Second_Count++;

			if(Hundred_Mili_Second_Count >=10){
				Hundred_Mili_Second_Count = 0;
				HUNDRED_MILI_SECONDS_SYSTICK_Callback();
				Second_Count++;

				if(Second_Count >=10){
					SECONDS_SYSTICK_Callback();
					Second_Count = 0;

				}
			}
		}
	}
}





