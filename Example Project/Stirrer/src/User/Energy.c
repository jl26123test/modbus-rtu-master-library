/*
 * Energy.c
 *
 *  Created on: Dec 8, 2017
 *      Author: angusschmaloer
 *
 */

#include "User/Energy.h"
#include <math.h>


extern signed int rpm;

bool Active = false;

const uint32_t    Power_Number    =   3380;           	//  3.380
const uint32_t    rho             = 998200;           	//998.200
const uint32_t    Diameter        =     75;     		//  0.075,000,000,000
const uint32_t    Viscosity       =      1002;        	//  0.001,002  m

uint64_t    VAL;
uint64_t 	G_Value;

void Energy_Calc_Active(bool active){Active = active;}

void Energy_Calc_Init(uint32_t Volume){
    uint64_t D = ((Diameter) * (Diameter));
    D *= Diameter;
    D *= Diameter;
    D *= Diameter;
    VAL = (Power_Number * rho)/1000;
    VAL = (VAL * D);
    VAL /= (Viscosity*Volume);

    G_Value = 0;
}

void Calc_Energy(int Time_Per_Second){
	if(Active == false){
		return;
	}
    double RPS = (double)rpm / 60;
    double Number = VAL * ((RPS*RPS*RPS));
    Number = sqrt(Number);
    Number /= Time_Per_Second;
    G_Value += (uint64_t)Number;
}

