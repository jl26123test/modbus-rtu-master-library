################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/User/Display.c \
../src/User/Energy.c \
../src/User/Modbus.c \
../src/User/ModbusRTU_Master.c \
../src/User/ModbusRTU_Slave.c \
../src/User/RotaryEncoder.c \
../src/User/Timer.c 

OBJS += \
./src/User/Display.o \
./src/User/Energy.o \
./src/User/Modbus.o \
./src/User/ModbusRTU_Master.o \
./src/User/ModbusRTU_Slave.o \
./src/User/RotaryEncoder.o \
./src/User/Timer.o 

C_DEPS += \
./src/User/Display.d \
./src/User/Energy.d \
./src/User/Modbus.d \
./src/User/ModbusRTU_Master.d \
./src/User/ModbusRTU_Slave.d \
./src/User/RotaryEncoder.d \
./src/User/Timer.d 


# Each subdirectory must supply rules for building sources it contributes
src/User/%.o: ../src/User/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DSTM32 -DSTM32F1 -DSTM32F103C8Tx -DDEBUG -DSTM32F103xB -DUSE_HAL_DRIVER -DUSE_FULL_LL_DRIVER -I"/home/angusschmaloer/workspace/Stirrer/HAL_Driver/Inc/Legacy" -I"/home/angusschmaloer/workspace/Stirrer/inc" -I"/home/angusschmaloer/workspace/Stirrer/CMSIS/device" -I"/home/angusschmaloer/workspace/Stirrer/CMSIS/core" -I"/home/angusschmaloer/workspace/Stirrer/HAL_Driver/Inc" -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


