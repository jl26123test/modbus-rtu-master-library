/*
 * Timer.h
 *
 *  Created on: Dec 8, 2017
 *      Author: angusschmaloer
 */

#ifndef USER_TIMER_H_
#define USER_TIMER_H_

#include <stdbool.h>
#include "stm32f1xx_hal.h"

enum SwitchState{
	Running,
	SettingUp,
	Updating
};

enum MenuState{
	RPM,
	Time,
	Volume,
	Start,
	Stop,
	_RPM,
	_Time,
	_Volume
};

TIM_HandleTypeDef	TIM2_Handle;
TIM_HandleTypeDef	TIM4_Handle;

void TIM2_Initialization(void);
void TIM4_Initialization(void);

void Timer4_Callback();

void Restart	(void);
void SetTime	(int *secs, int *mins, int *hours);
void StartTimer	(void);
void StopTimer	(void);
void Pause		(void);
void UpdateMotor(int RPM, bool _RPM);
void UpdateRPM	(signed int _freq);



#endif /* USER_TIMER_H_ */
