/*
 * Modbus.h
 *
 *  Created on: Nov 28, 2017
 *      Author: angusschmaloer
 */

#ifndef MODBUS_H_
#define MODBUS_H_

#include <stdint.h>

/* Struct for modbus error counters */
typedef struct{
	int CPT1;
	int CPT2;
	int CPT3;
	int CPT4;
	int CPT5;
	int CPT6;
	int CPT7;
	int CPT8;
}Error_counters;

/* Enum for the different modbus function codes */
enum Function_codes{
	Read_Coils						= 0x01,
	Write_Single_Coil 				= 0x05,
	Write_Multiple_Coils 			= 0x0F,
	Read_Discrete_Inputs			= 0x04,
	Read_Holding_Registers			= 0x03,
	Write_Single_Register			= 0x06,
	Write_Multiple_Registers		= 0x10,
	Read_Write_Multiple_Registers	= 0x17
	,Report_Slave_ID 				= 0x11
};

const uint16_t ModRTU_CRC(const uint8_t buf[], int len);

#endif /* MODBUS_H_ */
