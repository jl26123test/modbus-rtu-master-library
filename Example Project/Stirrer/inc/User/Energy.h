/*
 * Display.h
 *
 *  Created on: Dec 8, 2017
 *      Author: angusschmaloer
 */

#ifndef USER_ENERGY_H_
#define USER_ENERGY_H_

#include <stdbool.h>
#include <stdint.h>

void Energy_Calc_Init(uint32_t Volume);
void Calc_Energy(int Time_Per_Second);
void Energy_Calc_Active(bool active);

#endif /* USER_ENERGY_H_ */
