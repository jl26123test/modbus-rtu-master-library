/*
 * ModbusRTU_Slave.h
 *
 *  Created on: Nov 24, 2017
 *      Author: angusschmaloer
 */

#ifndef MODBUSRTU_SLAVE_H_
#define MODBUSRTU_SLAVE_H_

#include "stm32f1xx.h"
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_uart.h"
#include "stm32f1xx_hal_dma.h"
#include "stm32f1xx_ll_usart.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "User/Modbus.h"

#define MAXCLISTRING 256

/*
 * The Modbus_Slave_Data_Typedef struct contains the data that is available for the modbus slave.
 * Each data type has a pointer to the users data and the size of the user data
 */
typedef struct{
	uint16_t * Modbus_Slave_Holding_Registers;
	uint16_t   Holding_Registers_size;
	bool 	 * Modbus_Slave_Coils;
	uint16_t   Coils_Size;
	uint16_t * Modbus_Slave_Inputs;
	uint16_t   Input_Size;
	uint8_t  * Modbus_Slave_Discrete_Inputs;
	uint16_t   Discrete_Input_Size;
}Modbus_Slave_Data_Typedef;

/*
 * The ModbusRTU_Slave_Data_Typedef struct contains all data required for modbus slave communication.
 * 		Modbus_Slave_Data: 	Contains the data available for the modbus communication.
 * 		UART_Handle_Struct:	Contains the UART handle type, for the receive, send and stop functions.
 * 		Error_Counters:		Contains the different errors that might occur.
 * 		Data_String:		Contains the data sting that will be used for the communication, sending and receiving.
 * 		StringLen:			Contains the length of the data that will be send in the Data_String.
 * 		Slave_Address:		Contains the slave address of the slave devise.
 */
typedef struct{
	Modbus_Slave_Data_Typedef	* Modbus_Slave_Data;
	UART_HandleTypeDef 			* UART_Handle_Struct;
	Error_counters 			  	Error_Counters;
	uint8_t					  	Data_String[MAXCLISTRING];
	uint8_t					  	StringLen;
	uint8_t					  	Slave_Address;
}ModbusRTU_Slave_Data_Typedef;

/*
 * The slave data types that the user gives in the init function are stored here by using pointers.
 * When 2 modbusses are being used and there is an interrupt on uart 2, then the library used the UART2_ModbusRTU_Slave_Data_Struct pointer for processing the data.
 * The static callback functions can't use the users struct directly.
 * Thats why we use these two static pointers/
 * */
ModbusRTU_Slave_Data_Typedef *UART1_ModbusRTU_Slave_Data_Struct;
ModbusRTU_Slave_Data_Typedef *UART2_ModbusRTU_Slave_Data_Struct;

/* user functions */
int  ModbusRTU_Slave_Init	(ModbusRTU_Slave_Data_Typedef *ModbusRTU_Slave_Data_Struct);
void ModbusRTU_Slave_Start	(ModbusRTU_Slave_Data_Typedef *ModbusRTU_Slave_Data_Struct);
void ModbusRTU_Slave_Stop	(ModbusRTU_Slave_Data_Typedef *ModbusRTU_Slave_Data_Struct);

/* Callback handler function */
int  ModbusRTU_Slave_Handler(ModbusRTU_Slave_Data_Typedef *ModbusRTU_Slave_Data_Struct, int strlen);


#endif /* MODBUSRTU_SLAVE_H_ */
