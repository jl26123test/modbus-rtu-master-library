/*
 * UART.h
 *
 *  Created on: Nov 24, 2017
 *      Author: angusschmaloer
 */

#ifndef UART_H_
#define UART_H_


#include "stm32f1xx.h"
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_uart.h"
#include "stm32f1xx_hal_dma.h"
#include "stm32f1xx_ll_usart.h"

#define MAXCLISTRING 256

typedef struct{
	USART_TypeDef * UART;
	uint32_t baudrate;
	uint32_t stopbit;
	uint32_t parity;
	uint32_t wordlength;
}Serial_InitTypeDef;

int UART_Init(Serial_InitTypeDef * Serial, UART_HandleTypeDef * UART_HandleStruct);

DMA_HandleTypeDef UART1_tx_DMA_HandleStruct;
DMA_HandleTypeDef UART1_rx_DMA_HandleStruct;


#endif /* UART_H_ */
