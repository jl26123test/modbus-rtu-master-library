/*
 * ModbusRTU_Master.h
 *
 *  Created on: Nov 24, 2017
 *      Author: angusschmaloer
 */

#ifndef MODBUSRTU_MASTER_H_
#define MODBUSRTU_MASTER_H_

#define MAXMASTERCOMMANDS 12

#include "stm32f1xx.h"
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_uart.h"
#include "stm32f1xx_hal_dma.h"
#include "stm32f1xx_ll_usart.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "User/Modbus.h"

#define MAXCLISTRING 256

/*
 * The different states of a modbus master queue object
 */
typedef enum{
	processing,
	transmitted,
	received,
	succes,
	failed
} states;

/*
 * The ModbusRTU_Queue_Typedef struct contains the data that is available for each object in the queue.
 * Each object in the queue has one.
 * 		active:					Bool to set a command active or inactive for when the queue is handled
 * 		continuous:				Bool to indicate if this command is a continuous command or a one time command
 * 		retries:				The amount of retries the modbus command gets
 * 		timeout:				The max time the command gets before it timeouts (ms)
 * 		times_failed:			A counter that counts the temp fails (used for comparing it to the retries)
 * 		total_times_failed:		A counter that counts how many times this object in queue has failed
 * 		status:					The status of the modbus command
 * 		ErrorCode:				The error code of the modbus command
 *
 * 		Slave_Address:			The address of the slave you want to communicate with
 * 		Address:				The internal address of the slave where you want to write
 * 		data16:					A data pointer to the read/write data, uint16_t
 * 		data8:					A data pointer to the read/write data, uint8_t
 * 		data1:					A data pointer to the read/write data, bool
 *		Function_Codes:			The function code of the command
 */
typedef struct{
	bool 		active;
	bool 		continuous;
	int 		retries;
	int 		timeout;
	int 		times_failed;
	uint32_t 	total_times_failed;
	states 		status;
	int 		ErrorCode;

	uint8_t  	Slave_Address;
	uint16_t 	Address;
	uint16_t 	Quantity;
	uint16_t 	*data16;
	uint8_t  	*data8;
	bool	 	*data1;
	enum Function_codes Function_Codes;
}ModbusRTU_Queue_Typedef;

/*
 * The ModbusRTU_Master_Data_Typedef struct contains all data required for modbus Master communication.
 * 		ModbusRTU_Queue_Struct: A pointer to the modbus queue.
 * 		UART_Handle_Struct:		Contains the UART handle type, for the receive, send and stop functions.
 * 		Error_Counters:			Contains the different errors that might occur.
 * 		Data_String:			Contains the data sting that will be used for the communication, sending and receiving.
 * 		StringLen:				Contains the length of the data that will be send in the Data_String.
 * 		QueueCounter:			Contains the counter that count at which queue object we are.
 * 		queueSize:				Contains the total amount of queue objects in the queue
 * 		TimeoutCount:			Contains the systick counter at the point that the queue message is send
 * 		idle:					Contains if the modbus queue is idle of not.
 */
typedef struct{
	ModbusRTU_Queue_Typedef * ModbusRTU_Queue_Struct;
	UART_HandleTypeDef 		* UART_Handle_Struct;
	Error_counters 			  Error_Counters;
	uint8_t					  Data_String[MAXCLISTRING];
	uint8_t					  StringLen;
	uint8_t					  QueueCounter;
	uint8_t 				  queueSize;
	uint32_t 				  TimeoutCount;
	bool 					  idle;
}ModbusRTU_Master_Data_Typedef;

/*
 * The master data types that the user gives in the init function are stored here by using pointers.
 * When 2 modbusses are being used and there is an interrupt on uart 2, then the library used the ModbusRTU_Master_Data_Typedef pointer for processing the data.
 * The static callback functions can't use the users struct directly.
 * Thats why we use these two static pointers/
 * */
ModbusRTU_Master_Data_Typedef *UART1_ModbusRTU_Master_Data_Struct;
ModbusRTU_Master_Data_Typedef *UART2_ModbusRTU_Master_Data_Struct;


int ModbusRTU_Master_Init			(ModbusRTU_Master_Data_Typedef *ModbusRTU_Master_Data_Struct);
void ModbusRTU_Master_Start_Queue	(ModbusRTU_Master_Data_Typedef *ModbusRTU_Master_Data_Struct);
void ModbusRTU_Master_Handler		(ModbusRTU_Master_Data_Typedef * ModbusRTU_Master_Data_Struct, int strlen);
void ModbusRTU_Master_Timeout_Handler();

#endif /* MODBUSRTU_MASTER_H_ */
